<?php
/**
 * @file
 * Contains \Drupal\internetreception\Form\InternetReception.
 */

namespace Drupal\internetreception\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class InternetReception extends FormBase
{
/*
*
* {@inheritdoc}.
*/

    public function getFormId()
    {
        return 'internetreceptionform';
    }

    /**
     * {@inheritdoc}.
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $form['name'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Your name'),
            '#required' => TRUE
        );

        $form['email'] = array(
            '#type' => 'email',
            '#title' => $this->t('Your e-mail'),
            '#required' => TRUE
        );

        $form['age'] = array(
            '#type' => 'number',
            '#title' => $this->t('Your age'),
            '#required' => TRUE
        );

        $form['subject'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Subject'),
            '#required' => TRUE
        );

        $form['message'] = array(
            '#type' => 'textarea',
            '#title' => $this->t('Message'),
            '#required' => TRUE
        );

        $form['captcha'] = array('#type' => 'captcha');

        $form['actions']['#type'] = 'actions';
        // Добавляем нашу кнопку для отправки.
        $form['actions']['submit'] = array(
            '#type' => 'submit',
            '#value' => $this->t('Send'),
            '#button_type' => 'primary'
        );

        return $form;
    }
    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        if (strlen($form_state->getValue('name')) < 5) {
            $form_state->setErrorByName('name', $this->t('Name is too short.'));
        }

        if ((int)$form_state->getValue('age') < 0) {
            $form_state->setErrorByName('age', $this->t('Wrong age!'));
        } elseif ((int)$form_state->getValue('age') > 150) {
            $form_state->setErrorByName('age', $this->t('Wrong age!'));
        }
    }

    /**
     * @param array $form
     * @param FormStateInterface $form_state
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $mailManager = \Drupal::service('plugin.manager.mail');

        $module = 'internetreception';
        $key = 'create_article';
        $to = $form_state -> getValue('email');
        $params['message'] = $form_state -> getValue('message');
        $params['name'] = $form_state -> getValue('name');
        $params['subject'] = $form_state -> getValue('subject');
        $params['age'] = $form_state -> getValue('age');

        $langcode = \Drupal::currentUser()->getPreferredLangcode();
        $send = true;

        $result = $mailManager -> mail($module, $key, $to, $langcode, $params, NULL, $send);

        if ($result['result'] !== true) {
            drupal_set_message(t('There was a problem sending your message and it was not sent.'), 'error');
        }
        else {
            drupal_set_message(t('Your message has been sent.'));
        }
    }

}