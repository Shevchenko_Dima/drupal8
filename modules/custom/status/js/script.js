/**
 * @file
 */

(function($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.helloworld = {
    attach: function(context) {
      var $left = $(window).width() / 2 - drupalSettings.width / 2;
      var $top = $(window).height() / 2 - drupalSettings.height / 2;
      $('.block-custom-status').css({
        'width': drupalSettings.width,
        'height': drupalSettings.height,
        'left': $left,
        'top': $top,
        });

      var $top_area = 35;
      $('.status-message').css({
          'width': drupalSettings.width,
          'height': drupalSettings.height - $top_area,
        }).scroll();

      $('.status-title').append(drupalSettings.title);

      $('.overlay').show();
      $('#block-custom-status--draggable').show();

      $.fn.drags = function(opt) {
        opt = $.extend({handle: '', cursor: 'move',}, opt);
        if (opt.handle === '') {
          var $el = this;
        }
        else {
          var $el = this.find(opt.handle);
        }
        return $el.css('cursor', opt.cursor).on('mousedown', function(e) {
          if (opt.handle === '') {
            var $drag = $(this).addClass('draggable');
          }
          else {
            var $drag = $(this)
              .addClass('active-handle')
              .parent()
              .addClass('draggable');
          }

          var $z_idx = $drag.css('z-index');
          var $drg_h = $drag.outerHeight();
          var $drg_w  = $drag.outerWidth();
          var $pos_y = $drag.offset().top + $drg_h - e.pageY;
          var $pos_x = $drag.offset().left + $drg_w  - e.pageX;
          $drag.css('z-index', 20).parents().on('mousemove', function(e) {
            $('.draggable').offset({
              top: e.pageY + $pos_y - $drg_h,
              left: e.pageX + $pos_x - $drg_w
                }).on('mouseup', function() {
                  $(this).removeClass('draggable').css('z-index', $z_idx);
                });
          });
          e.preventDefault();
        }).on('mouseup', function() {
          if (opt.handle === '') {
            $(this).removeClass('draggable');
          }
          else {
            $(this)
              .removeClass('active-handle')
              .parent()
              .removeClass('draggable');
          }
        });
      };
      $('#block-custom-status--draggable').drags();

      $('.block-custom-status #status-button--close').click(function() {
        $('.overlay').hide();
        $('.block-custom-status').hide();
      });

      $('.overlay').click(function() {
        $('.overlay').hide();
          $('.block-custom-status').hide();
      })
    }
  }
})(jQuery, Drupal, drupalSettings);