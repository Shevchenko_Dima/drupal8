<?php

namespace Drupal\note\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;

/**
 * Class NoteSettingForm.
 *
 * @package Drupal\note\Form
 */
class NoteSettingForm extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'note_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['note.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['date'] = [
      '#type' => 'date',
      '#title' => 'Date',
      '#description' => 'Enter the date.',
      '#required' => true,
    ];
    $form['sent'] = [
      '#type' => 'submit',
      '#value' => $this->t('Change Date'),
    ];
    return $form;
  }
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config = $this->config('note.settings');
    $config->set('setting_date', $form_state->getValue('date'));
    $config->save();
    $nids = \Drupal::entityQuery('node')
      ->condition('type', 'note')
      ->sort('created', 'ASC')
      ->execute();
    $batch = [
      'title' => t('Change'),
      'operations' => [
        [
          '\Drupal\note\DateNode::noneNodeStatus', [$nids],
        ],
        [
          '\Drupal\note\DateNode::changeNodeStatus', [$nids]
        ],

      ],
      'finished' => '\Drupal\note\DateNode::changeNodeStatusFinishedCallback',
    ];
    batch_set($batch);
  }
}