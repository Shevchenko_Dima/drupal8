<?php

namespace Drupal\note;

use Drupal\node\Entity\Node;
use Drupal\Core\Config\Config;

class DateNode extends Config {

  public static function noneNodeStatus($nids, &$context){
    $message = 'Resetting the status field...';
    $results = [];
    foreach ($nids as $nid) {
      $node = Node::load($nid);
      $node->field_status = 'N/A';
      $results = $node ->save();
    }
    $context['message'] = $message;
    $context['results'] = $results;
  }

  public static function changeNodeStatus($nids, &$context){
    $config = \Drupal::config('note.settings');
    $current_date = strtotime($config->get('setting_date'));
    $message = 'Update Status Field...';
    $results = [];
    foreach ($nids as $nid) {
      $node = Node::load($nid);
      $date = $node->getCreatedTime();

      if ($current_date > $date) {
        $node->field_status = 'Expired';
      }
      else {
        $node->field_status = 'Actual';
      }
      $results = $node ->save();
    }
    $context['message'] = $message;
    $context['results'] = $results;
  }
  public static function changeNodeStatusFinishedCallback($success, $results, $operations) {
    // The 'success' parameter means no fatal PHP errors were detected. All
    // other error management should be handled using 'results'.
    if ($success) {
      $message = 'Success!';
    }
    else {
      $message = t('Finished with an error.');
    }
    drupal_set_message($message);
  }
}