<?php
/**
 * @file
 * Contains \Drupal\newsletter\Form\NewsletterAdminForm.
 */

namespace Drupal\newsletter\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Render\FormattableMarkup;

/**
 * Configure settings for this site.
 */
class NewsletterAdminForm extends ConfigFormBase {

  /**
   * Gets the configuration names that will be editable.
   *
   * @return array
   *   An array of configuration object names that are editable if called in
   *   conjunction with the trait's config() method.
   */
  protected function getEditableConfigNames() {
    return ['newsletter.settings'];
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'newsletter_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $queue = \Drupal::queue('newsletter_mass_sending');
    if ($number_of_items = $queue->numberOfItems()) {
      $form['info_text'] = [
        '#type' => 'markup',
        '#markup' => new FormattableMarkup('<div>This queue is already running, there are still: @number</div>', [
          '@number' => $number_of_items,
        ]),
      ];

      $form['delete'] = [
        '#type' => 'submit',
        '#value' => $this->t('Cancel current queue'),
        '#disable' => TRUE,
      ];
    }
    else {
      $form['message'] = [
        '#type' => 'textarea',
        '#title' => 'Message',
        '#default_value' => 'Hi [user:name] of the [site:mail]',
        '#description' => 'Use  [user:name] , [site:mail].',
        '#required' => false,
        ];
      $form['button'] = [
        '#type' => 'submit',
        '#value' => $this->t('Sent'),
        '#disable' => TRUE,
        ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('newsletter.settings');
    $config->set('message', $form_state->getValue('message'));
    $config->save();
    $queue = \Drupal::queue('newsletter_mass_sending');
    if ($form_state->getTriggeringElement()['#id'] == 'edit-delete') {
      $queue->deleteQueue();
    }
    else {
      $query = \Drupal::database()->select('users_field_data', 'u')
        ->fields('u', ['name', 'mail'])
        ->condition('u.status', 1);
      $result = $query->execute();
      $queue->createQueue();
      foreach ($result as $row) {
        $queue->createItem([
          'name' => $row->name,
          'mail' => $row->mail,
          ]);
      }
    }
  }
}