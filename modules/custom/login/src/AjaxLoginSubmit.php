<?php

/**
 * @file
 * Contains \Drupal\login\AjaxLoginSubmit.
 */

namespace Drupal\login\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class AjaxContactSubmit
 * @package Drupal\dummy\AjaxContactSubmit
 */
class AjaxLoginSubmit {

    /**
     * Ajax form submit callback.
     *
     * @param array $form
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     * @return \Drupal\Core\Ajax\AjaxResponse
     */
    public function ajaxSubmitCallback(array &$form, FormStateInterface $form_state) {

        $ajax_response = new AjaxResponse();
        $username = $form_state -> getValue('name');

       //if( $form_state -> get(uid)){
           drupal_set_message(t('Hi, @name', ['@name' => $username]));
       // }

        $message = [
            '#theme' => 'status_messages',
            '#message_list' => drupal_get_messages(),
            '#status_headings' => [
                'status' => t('Status message'),
                'error' => t('Error message'),
                'warning' => t('Warning message'),
            ],
        ];
        $messages = \Drupal::service('renderer')->render($message);
        $ajax_response->addCommand(new HtmlCommand('#' . Html::getClass($form['form_id']['#value']) . '-messages', $messages));
        return $ajax_response;
    }
}