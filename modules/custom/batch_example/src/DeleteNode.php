<?php
namespace Drupal\batch_example;
use Drupal\node\Entity\Node;
class DeleteNode {
  public static function deleteNodeExample($nids, &$context){
    $message = 'Change Node...';
    $results = [];
    foreach ($nids as $nid) {
      $node = Node::load($nid);
      $node->field_status = 'Actual';
      $results = $node ->save();
    }
    $context['message'] = $message;
    $context['results'] = $results;
  }
  public static function deleteNodeExampleFinishedCallback($success, $results, $operations) {
    // The 'success' parameter means no fatal PHP errors were detected. All
    // other error management should be handled using 'results'.
    if ($success) {
      $message = \Drupal::translation()->formatPlural(
        count($results),
        'One post processed.', '@count posts processed.'
      );
    }
    else {
      $message = t('Finished with an error.');
    }
    drupal_set_message($message);
  }
}