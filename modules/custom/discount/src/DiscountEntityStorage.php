<?php

namespace Drupal\discount;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\discount\Entity\DiscountEntityInterface;

/**
 * Defines the storage handler class for Discount entity entities.
 *
 * This extends the base storage class, adding required special handling for
 * Discount entity entities.
 *
 * @ingroup discount
 */
class DiscountEntityStorage extends SqlContentEntityStorage implements DiscountEntityStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(DiscountEntityInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {discount_entity_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {discount_entity_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(DiscountEntityInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {discount_entity_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('discount_entity_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
