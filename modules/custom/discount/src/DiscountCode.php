<?php

/**
 * @file
 * Contains \Drupal\discount\Controller\DiscountCode.
 */

namespace Drupal\discount;

use Drupal\Core\Controller\ControllerBase;

class DiscountCode extends ControllerBase {
  public function createDiscountCode(){
    $discount_code = substr(md5(uniqid(rand(),true)) , 22);
    return $discount_code;
  }
}