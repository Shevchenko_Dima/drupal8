<?php

namespace Drupal\discount;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for discount_entity.
 */
class DiscountEntityTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
