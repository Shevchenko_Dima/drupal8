<?php

/**
 * @file
 * Contains \Drupal\discount\Form\DiscountSettingForm.
 *
 * Form for discount settings.
 */

namespace Drupal\discount\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure example settings for this site.
 */
class DiscountSettingForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'discount_configuration_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['discount.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';
    $form['message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Status'),
      '#default_value' => 'Hello, [current-user:name]! Your discount code is [current-user:discount_code].',
      '#description' => 'Use [current-user:name], [current-user:discount_code].',
      '#required' => true,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $token_service = \Drupal::token();
    $config = $this->config('discount.settings');
    $flag = $form_state->getValue('message');
    if($flag) {
      $config->set('message', $flag)->save();
      $message = $config ->get('message');
      $message = $token_service->replace($message);
      drupal_set_message($message);
    }
  }
}