<?php

namespace Drupal\discount\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Discount entity entities.
 *
 * @ingroup discount
 */
class DiscountEntityDeleteForm extends ContentEntityDeleteForm {


}
