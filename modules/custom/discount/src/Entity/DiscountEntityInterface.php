<?php

namespace Drupal\discount\Entity;

use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Discount entity entities.
 *
 * @ingroup discount
 */
interface DiscountEntityInterface extends RevisionableInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Discount entity name.
   *
   * @return string
   *   Name of the Discount entity.
   */
  public function getName();

  /**
   * Sets the Discount entity name.
   *
   * @param string $name
   *   The Discount entity name.
   *
   * @return \Drupal\discount\Entity\DiscountEntityInterface
   *   The called Discount entity entity.
   */
  public function setName($name);

  /**
   * Gets the Discount entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Discount entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Discount entity creation timestamp.
   *
   * @param int $timestamp
   *   The Discount entity creation timestamp.
   *
   * @return \Drupal\discount\Entity\DiscountEntityInterface
   *   The called Discount entity entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Discount entity published status indicator.
   *
   * Unpublished Discount entity are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Discount entity is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Discount entity.
   *
   * @param bool $published
   *   TRUE to set this Discount entity to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\discount\Entity\DiscountEntityInterface
   *   The called Discount entity entity.
   */
  public function setPublished($published);

  /**
   * Gets the Discount entity revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Discount entity revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\discount\Entity\DiscountEntityInterface
   *   The called Discount entity entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Discount entity revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Discount entity revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\discount\Entity\DiscountEntityInterface
   *   The called Discount entity entity.
   */
  public function setRevisionUserId($uid);

}
