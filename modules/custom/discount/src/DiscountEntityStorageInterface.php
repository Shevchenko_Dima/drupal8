<?php

namespace Drupal\discount;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\discount\Entity\DiscountEntityInterface;

/**
 * Defines the storage handler class for Discount entity entities.
 *
 * This extends the base storage class, adding required special handling for
 * Discount entity entities.
 *
 * @ingroup discount
 */
interface DiscountEntityStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Discount entity revision IDs for a specific Discount entity.
   *
   * @param \Drupal\discount\Entity\DiscountEntityInterface $entity
   *   The Discount entity entity.
   *
   * @return int[]
   *   Discount entity revision IDs (in ascending order).
   */
  public function revisionIds(DiscountEntityInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Discount entity author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Discount entity revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\discount\Entity\DiscountEntityInterface $entity
   *   The Discount entity entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(DiscountEntityInterface $entity);

  /**
   * Unsets the language for all Discount entity with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
