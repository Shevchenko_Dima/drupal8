<?php

namespace Drupal\discount\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\discount\Entity\DiscountEntityInterface;

/**
 * Class DiscountEntityController.
 *
 *  Returns responses for Discount entity routes.
 */
class DiscountEntityController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a Discount entity  revision.
   *
   * @param int $discount_entity_revision
   *   The Discount entity  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($discount_entity_revision) {
    $discount_entity = $this->entityManager()->getStorage('discount_entity')->loadRevision($discount_entity_revision);
    $view_builder = $this->entityManager()->getViewBuilder('discount_entity');

    return $view_builder->view($discount_entity);
  }

  /**
   * Page title callback for a Discount entity  revision.
   *
   * @param int $discount_entity_revision
   *   The Discount entity  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($discount_entity_revision) {
    $discount_entity = $this->entityManager()->getStorage('discount_entity')->loadRevision($discount_entity_revision);
    return $this->t('Revision of %title from %date', ['%title' => $discount_entity->label(), '%date' => format_date($discount_entity->getRevisionCreationTime())]);
  }

  /**
   * Generates an overview table of older revisions of a Discount entity .
   *
   * @param \Drupal\discount\Entity\DiscountEntityInterface $discount_entity
   *   A Discount entity  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(DiscountEntityInterface $discount_entity) {
    $account = $this->currentUser();
    $langcode = $discount_entity->language()->getId();
    $langname = $discount_entity->language()->getName();
    $languages = $discount_entity->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $discount_entity_storage = $this->entityManager()->getStorage('discount_entity');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $discount_entity->label()]) : $this->t('Revisions for %title', ['%title' => $discount_entity->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all discount entity revisions") || $account->hasPermission('administer discount entity entities')));
    $delete_permission = (($account->hasPermission("delete all discount entity revisions") || $account->hasPermission('administer discount entity entities')));

    $rows = [];

    $vids = $discount_entity_storage->revisionIds($discount_entity);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\discount\DiscountEntityInterface $revision */
      $revision = $discount_entity_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $discount_entity->getRevisionId()) {
          $link = $this->l($date, new Url('entity.discount_entity.revision', ['discount_entity' => $discount_entity->id(), 'discount_entity_revision' => $vid]));
        }
        else {
          $link = $discount_entity->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.discount_entity.translation_revert', ['discount_entity' => $discount_entity->id(), 'discount_entity_revision' => $vid, 'langcode' => $langcode]) :
              Url::fromRoute('entity.discount_entity.revision_revert', ['discount_entity' => $discount_entity->id(), 'discount_entity_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.discount_entity.revision_delete', ['discount_entity' => $discount_entity->id(), 'discount_entity_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['discount_entity_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
