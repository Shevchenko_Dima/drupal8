<?php

namespace Drupal\discount\Controller;

/**
 * Class DiscountPrivateInformationController.
 *
 *  Returns responses discount code in title.
 */
class DiscountPrivateInformationController {

  /**
   * Returns a page render array.
   */
  public function getContent() {
    $build = [];
    if(\Drupal::currentUser()->id()) {
      $token_service = \Drupal::token();
      $message = '[current-user:name], your discount code: [current-user:discount_code]';
      $message = $token_service->replace($message);
      $build['#markup'] = $message;
    }
    else{
      $message = 'Please login or register';
      $build['#markup'] = $message;
    }
      return $build;
  }
}
